# README #

Dette lille projekt demonstrerer en helt simpel Hello World Web Server med Node og TypeScript. Projektet kan nemt oprettes i Visual Studio Code.

### Opsætning ###

Kontrollér at Node og TypeScript er installeret:
`node --version`
`tsc --version`


Opret en folder med følgende to underfoldere:

* build
* server


### Konfiguration ###

Opret en **package.json** ved at køre følgende:

`npm init -y`

Installér en *DefinitelyTyped* ved at køre følgende fra application-roden:

`npm install --save-dev @types/node`

[Se evt. mere her](https://github.com/DefinitelyTyped/DefinitelyTyped )

I projektroden oprettes filen **tsconfig.json**:

```
{
    "compilerOptions": {
        "target": "es5",
        "module": "commonjs",
        "emitDecoratorMetadata": true,
        "experimentalDecorators": true,
        "outDir": "build"
    },
    "exclude": [
        "node_modules",
        "client"
    ]
}
```


### Serveren ###

I server-folderen oprettes filen **hello_server.ts**:
```
import * as http from 'http';
const server = http.createServer((request, response) => {
    response.writeHead(200, {'Content-Type': 'text/plain'});
    response.end('Hello World!\n');
});
const port = 8000;
server.listen(port);
console.log('Listening on http://localhost:' + port);

```
### Build og Run ###

Build projektet ved i projektroden at skrive: 
`
tsc
`

Kør projektet ved at skifte til build-folderen og skrive:
`
node hello_server.js
`

Test i en browser ved at webbe: 
`
http://localhost:8000
`